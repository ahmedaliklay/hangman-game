//Letters 
const letters = "qwertyuioplkjhgfdsazxcvbnm";

//Get Array From Letters
let lettersArray = Array.from(letters);

//console.log(lettersArray);

//Select Letters Containers
let lettersContainer = document.querySelector('.letters');

//Gernrate Letters 

lettersArray.forEach(letters => {

    //Create Span
    let span = document.createElement("span");

    //Create Letter Text Node  
    let  theLetter = document.createTextNode(letters);

    //Append Letter To Span
    span.appendChild(theLetter);

    //Add Class On Span
    span.className  = "letter-box";

    //Append Span To The Letters Container
    lettersContainer.appendChild(span);
});

//Create Object Of Words + categories
const words = {
    programming:    ["HTML","CSS","JavaScript","Sass","Php","Php Laravel","Node js","Php wordpress","python"],
    movies:         ["Departed","Heat","Heart","Hope","TheDarkKnight"],
    people:         ["Ahmed Ali","Ali Klay","Klay","Osama ElZero"],
    countries:      ["palestine","Yemen","Syria"]
}

//Get Random Proparty

let allKeys = Object.keys(words);

//Random Number Deppend On Keys Length
let randomPropNumber    = Math.floor(Math.random() * allKeys.length);

//Category
let randomPropName      = allKeys[randomPropNumber];

//category Words
let randomPropValue     = words[randomPropName]; 


let randomValueNumber    = Math.floor(Math.random() * randomPropValue.length);
let randomValueValue     = randomPropValue[randomValueNumber];

//console.log(randomValueValue);

//Set  category info
document.querySelector(".game-info .category span").innerHTML = randomPropName ;
//+ " : " + randomValueValue

//Select Letters Guess Element
let lettersGuessContainer = document.querySelector(".letters-guess");

//Convert Chose Word To Array
let lettersAndSpan = Array.from(randomValueValue);

//create spans Depened On Word
lettersAndSpan.forEach(letter =>{
    //create Empty Span
    let emptySpan = document.createElement("Span");

    //if letter is space
    if(letter == ' '){
        //Add Class to The span
        emptySpan.className = "has-space";
    }
    //Append Span To Guess Container
    lettersGuessContainer.appendChild(emptySpan);
});

//Select Guess spans
let guessSpans = document.querySelectorAll(".letters-guess span");

//Set Wrong Attempts
let wrongAttempts = 0;

//Select the draw Element
let theDraw = document.querySelector(".hangMan-draw");

//Sounds
var     successSound = document.getElementById('success'),
        failSound = document.getElementById('fail');
//End Sound

//Hendle clicking on letters
document.addEventListener("click",function(e){

    //Set the chose status
    let theStatus = false;


    if(e.target.className === "letter-box"){

        e.target.classList.add("clicked");

        //Get Clicked letter
        let theClickedLetter = e.target.innerHTML.toLowerCase();

        //Chose Word
        let theChosenWord = Array.from(randomValueValue.toLowerCase());

        //
        theChosenWord.forEach((wordLetter,wordindex) =>{

            //if the clicked letter Equal To One Of The chosen word letter
            if(theClickedLetter == wordLetter){
                //console.log(index);
                //Set Status To Correct [theStatus]
                theStatus = true;

                //loop On All Guess Span
                guessSpans.forEach((span,spanIndex)=>{
                    if(wordindex === spanIndex){

                        span.innerHTML = theClickedLetter;

                    }//End If

                });//End Foraech
            }

        });//End Foreach

        //if letter is wrong
        if(theStatus !== true){
            //Increase the wrong Attempts
            wrongAttempts++;

            //Add Class Wrong  ON The Draw Element
            theDraw.classList.add(`wrong-${wrongAttempts}`);

            //play fail sound
            failSound.play();
            if(wrongAttempts === 8){
                endGame();
                lettersContainer.classList.add("finished");
            }
        }else{
            //play successSound sound
            successSound.play();
        }
    }

});
//End Game Function
function endGame(){
    //create poupup Div
    let div = document.createElement("div");
    //create span
    let span = document.createElement("span");

    //create text
    let spanText = document.createTextNode(`Game Over , The Word Is ${randomValueValue}`);

    //append text To Div 
    div.appendChild(span);
    //append text To Div 
    span.appendChild(spanText);
    

    //Add class On Div
    div.className = "poupup";

    //Append to the body
    document.body.appendChild(div);
}